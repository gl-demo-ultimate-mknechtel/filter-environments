# Filter Environments

## What it does
Obtains a list of deployments within a specified time range that are pushed to each project's production environment in a group.

## How to use
To use this, create a PAT and run: `python3 filter_environments.py {PAT} {GROUP_ID} {UTC START TIME} {UTC END TIME}`. The script will return the deployments to production in the time range. You can modify the script to print this information to a CSV or other output of your choice. Example run: `python3 filter_environments.py {PAT} {GROUP_ID} 2023-01-18T22:17:01.042Z 2023-01-18T22:17:40.881Z`