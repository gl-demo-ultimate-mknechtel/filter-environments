from logging import exception
from os import environ
import gitlab
import sys


def list_deployments(gl, projects_list, start_date, end_date):
    prod_deployments_in_range = []
    for project in projects_list:
        project = gl.projects.get(project.id)
        try:
            deployments = project.deployments.list(get_all=True)
            prod_deployments = [d for d in deployments if d.environment['name'] == "production" and 'started_at' in d.deployable and 'finished_at' in d.deployable and d.status == "success"]
            prod_deployments_in_range += [d for d in prod_deployments if d.deployable["started_at"] >= start_date and d.deployable["finished_at"] <= end_date]
        except Exception as e:
            print(e)
    return prod_deployments_in_range


def get_projects(gl, group_id):
    group = gl.groups.get(group_id)
    return group.projects.list(all=True, include_subgroups=True)


def authenticate(pat):
    gl = gitlab.Gitlab('https://gitlab.com', private_token=pat)
    print("Begin authenticating...")
    gl.auth()
    print("Authentication successful!")
    return gl


def main():
    pat = sys.argv[1]
    group_id = sys.argv[2]
    start_date = sys.argv[3]
    end_date = sys.argv[4]
    
    gl = authenticate(pat)
    projects_list = get_projects(gl, group_id)
    prod_deployments_in_range = list_deployments(gl, projects_list, start_date, end_date)
    # Print production deployment in time range
    print(prod_deployments_in_range)

main()
